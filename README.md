# Airflow Demo

## Running Airflow

- [Install Docker](https://docs.docker.com/get-docker/)

- From project directory, run `docker compose up`

- Airflow can be accessed on `localhost:8080`
