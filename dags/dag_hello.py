import time
from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator


def hello(name, wait=False, **kwargs):
    if wait:
        time.sleep(5)
        raise ValueError
    return f"Hello {name}"


def goodbye():
    return "Goodbye"


def failsafe():
    return "Deu ruim"


with DAG(
    "hello",
    schedule_interval="0 * * * *",
    start_date=days_ago(1),
    catchup=False,
    max_active_runs=1,
) as dag:
    hello_task = PythonOperator(
        task_id="hello", python_callable=hello, op_args=["Everyone"]
    )

    goodbye = PythonOperator(task_id="goodbye", python_callable=goodbye)

    people = ["Elaine", "André"]
    igor = PythonOperator(task_id="hello_i", python_callable=hello, op_args=["Igor"])
    tulio = PythonOperator(task_id="hello_t", python_callable=hello, op_args=["Túlio"])
    diego = PythonOperator(
        task_id="hello_d", python_callable=hello, op_args=["Diego", True]
    )
    tasks_people = []
    for person in people:
        tasks_people.append(
            PythonOperator(
                task_id=f"hello_{person[0].lower()}",
                python_callable=hello,
                op_args=[person],
            )
        )

    wait = BashOperator(task_id="wait", bash_command="sleep 5")

    fail = PythonOperator(
        task_id="failsafe", python_callable=failsafe, trigger_rule="all_done"
    )

    hello_task >> wait >> [igor, tulio, diego] >> goodbye
    [igor, tulio, diego] >> fail
    wait >> tasks_people >> goodbye
